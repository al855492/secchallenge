package it.unive.challenge.portal.filters;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SecurityFilter implements Filter {
	private Pattern pattern;
	/**
	* @see Filter#init(FilterConfig)
	*/ 
	public void init(FilterConfig fConfig) throws ServletException {
		String regexp = fConfig.getInitParameter("regexp");
		
		// Loading authorized path from filter parameters
		if (regexp != null) {
			this.pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE);
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest rqs = (HttpServletRequest) request;
		HttpServletResponse rsp = (HttpServletResponse) response;
		
		String action = rqs.getServletPath();
		
		// For security reasons, users can not access directly to WebContent resources
		// We want that all requests pass through our super secure servlet
		if (action == null || (this.pattern != null && !this.pattern.matcher(action).matches())) {
			// In case of direct access we return a 404 error
			rsp.sendError(HttpServletResponse.SC_NOT_FOUND, rqs.getRequestURI());
		} else chain.doFilter(request, response);
	}
}
