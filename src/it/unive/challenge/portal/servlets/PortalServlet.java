package it.unive.challenge.portal.servlets;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unive.challenge.ServletUtils;

public class PortalServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5876908502702752835L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String page = request.getParameter("page");
		
		// Handling page received as input
		if (page == null) {
			// Default case is page=home
			ServletUtils.sendRedirect(request, response, request.getServletPath() + "?page=home");
		} else if (isValidPage(page)) {
			// The page exists so we forward the request to site.jsp
			ServletUtils.sendForward(request, response, "/site.jsp?include=" + page);
		} else {
			// Invalid page or page not found on file system. We send back the notfound page.
			ServletUtils.sendForward(request, response, request.getServletPath() + "?page=notfound");
		}
	}
	
	/**
	 * Checking if a page exists in our filesystem
	 * @param page
	 * @return
	 */
	private boolean isValidPage(String page) {
		boolean valid = false;
		
		if (page != null) {
			// Reading file from file system
			String pagePath = (this.getServletContext().getRealPath("pages") + File.separator + page + ".jsp");
			File pageFile = new File(pagePath);
			
			// Check if file exists
			if (pageFile.exists()) {
				try {
					// We want to prevent 'Path Traversal' attacks.
					// So we check if the canonical path is equals to the one provided as input
					valid = pagePath.equals(pageFile.getCanonicalPath());
					
				} catch (IOException e) { valid = false; }
			}
		}
		
		return valid;	
	}

}
