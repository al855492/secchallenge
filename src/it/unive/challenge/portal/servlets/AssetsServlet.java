package it.unive.challenge.portal.servlets;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unive.challenge.MimeTypeDetector;

public class AssetsServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3694959304494308117L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String file = request.getParameter("file");
		
		if (file != null) {
			// Normalizing file name
			file = file.replace("/", File.separator);
			if (file.contains("?")) {
				file = file.substring(0, file.lastIndexOf("?"));
			}
			if (!file.startsWith(File.separator)) {
				file = File.separator + file;
			}
			
			String webPath = this.getServletContext().getRealPath("");
			String assetPath = webPath + File.separator + "assets" + file;
			File assetFile = new File(assetPath);
			
			// The file asset in the request must exists
			if (assetFile.exists()) {
				// For security reasons users cannot read files outside the application folder!
				String filePath = assetFile.getPath();
				
				// So we check if the asset file belongs to WebContent folder
				if (filePath.startsWith(webPath)) {
					// Sending content-type
					response.setHeader("Content-Type", MimeTypeDetector.getMimeType(assetFile));
					
					// Sending file content
					Files.copy(assetFile.toPath(), response.getOutputStream());
				} else {
					response.sendError(HttpServletResponse.SC_FORBIDDEN, request.getRequestURI());
				}
			} else {
				response.sendError(HttpServletResponse.SC_NOT_FOUND, "File not found: " + assetPath);
			}			
		} else {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI());
		}		
	}
}
