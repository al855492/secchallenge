package it.unive.challenge.portal.servlets;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RobotsServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8731067600751264725L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// Hiding to bots the address of admin's rest services
		response.getWriter().print("User-agent: *\nDisallow: " + request.getContextPath() + "/SecureCMS/rest/*");
	}
}
