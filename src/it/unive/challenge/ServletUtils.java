package it.unive.challenge;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

public class ServletUtils {
	private ServletUtils() {}
	
	/**
	 * Send an application Forward to forward string
	 * @param request
	 * @param response
	 * @param forward
	 * @throws ServletException
	 * @throws IOException
	 */
	public static void sendForward(HttpServletRequest request, HttpServletResponse response, String forward) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(forward);
		dispatcher.forward(request, response);
	}
	
	/**
	 * Send an application Redirect to redirect string
	 * @param request
	 * @param response
	 * @param redirect
	 * @throws IOException
	 */
	public static void sendRedirect(HttpServletRequest request, HttpServletResponse response, String redirect) throws IOException {
		// Always appending contextpath
		if (!redirect.startsWith(request.getContextPath())) {
			redirect = request.getContextPath() + redirect;
		}
		
		response.sendRedirect(redirect);
	}
	
	/**
	 * Send a json as http response
	 * @param request
	 * @param response
	 * @param json
	 * @throws IOException
	 */
	public static void sendJSON(HttpServletRequest request, HttpServletResponse response, JSONObject json) throws IOException {
		// Prevent browser caching
		response.setHeader("Content-Type", "application/json");
		response.setHeader("Cache-Control", "no-store");
		
		response.getWriter().write(json.toString());
	}
	
	/**
	 * Send a json error as response
	 * @param request
	 * @param response
	 * @param statusCode
	 * @param error
	 * @throws IOException
	 */
	public static void sendJSONError(HttpServletRequest request, HttpServletResponse response, int statusCode, String error) throws IOException {
		// Preparing json object to be returned
		JSONObject json = new JSONObject();
		json.put("result", "KO");
		json.put("error", error); // setting error message 
		
		// Setting status code
		response.setStatus(statusCode);
		
		// Send data
		sendJSON(request, response, json);
	}
	
	/**
	 *  Send a json success as response
	 * @param request
	 * @param response
	 * @param message
	 * @throws IOException
	 */
	public static void sendJSONSuccess(HttpServletRequest request, HttpServletResponse response, String message) throws IOException {
		// Preparing json object to be returned
		JSONObject json = new JSONObject();
		json.put("result", "OK");
		json.put("message", message); // setting output message 
		
		// Always 200
		response.setStatus(HttpServletResponse.SC_OK);
		
		// Send data
		sendJSON(request, response, json);	
	}
}
