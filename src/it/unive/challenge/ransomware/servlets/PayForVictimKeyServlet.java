package it.unive.challenge.ransomware.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unive.challenge.ServletUtils;

public class PayForVictimKeyServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3293268238221248809L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uuid = request.getParameter("uuid");
		
		if (uuid != null) {
			String privateKey = request.getParameter("privateKey");
			
			if (privateKey != null) {
				ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_EXPECTATION_FAILED, "Unable to complete the transaction of 500 BTC. AES Key used for file's encryption has been deleted and files lost forever.");
			} else ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST, "Missing parameter: privateKey");
		} else ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST, "Missing parameter: uuid");		
	}
}
