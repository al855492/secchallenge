package it.unive.challenge.admin.servlets.rest;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import it.unive.challenge.ServletUtils;
import it.unive.challenge.admin.servlets.AuthenticatedServlet;

public class AssetsListCMSServlet extends AuthenticatedServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6790901596820002413L;

	@Override
	public void doAuthenticatedGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String dir = request.getParameter("dir");
		if (dir == null) dir = "";
		
		// Normalize path to file system
		dir = dir.replace("/", File.separator);
		dir = dir.replace("\\", File.separator);
		
		String basePath = this.getServletContext().getRealPath("");
		// By default all requests starts from 'assets' folder
		File dirPath = new File(this.getServletContext().getRealPath("assets") + File.separator + dir);
		
		// Checking if the path is inside web applications
		String canonicalPath = dirPath.getCanonicalPath();
		if (!canonicalPath.endsWith(File.separator))
			canonicalPath += File.separator;
		
		if (canonicalPath.startsWith(basePath)) {
			// Path must exists
			if (dirPath.exists()) {
				// It must be a directory
				if (dirPath.isDirectory()) {
					JSONObject json = new JSONObject();
					json.put("dir", "/assets/" + dir.replace(File.separator, "/"));
					
					JSONArray jFiles = new JSONArray();
					
					// We list all the file contained inside the current directory
					for(String file : dirPath.list()) {
						JSONObject jFile = new JSONObject();
						File tmp = new File(dirPath + File.separator + file);
						
						jFile.put("name", file); // file name
						jFile.put("type", tmp.isDirectory() ? "dir" : "file"); // file type (dir/file)						
						
						jFiles.put(jFile);
					}
					
					json.put("files", jFiles);
					
					ServletUtils.sendJSON(request, response, json);
				} else {
					ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST, dir.replace(File.separator, "/") + " isn't a valid directory");
				}
			} else {
				ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST, "directory " + dir.replace(File.separator, "/") + " doesn't exist");
			}			
		} else {
			ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_FORBIDDEN, dir.replace(File.separator, "/") + " is outside the WebContent directory");
		}
		

	}
}
