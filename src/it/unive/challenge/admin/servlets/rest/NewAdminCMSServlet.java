package it.unive.challenge.admin.servlets.rest;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unive.challenge.ServletUtils;
import it.unive.challenge.admin.servlets.AuthenticatedServlet;

public class NewAdminCMSServlet extends AuthenticatedServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7036650400070581504L;

	// In this set we save the information of all registered users (username/email)
	private static Set<String> usersInfo = new LinkedHashSet<>();
	
	// Pattern used in email validation
	private final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	
	@Override
	public void doAuthenticatedPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		
		// username field is mandatory
		if (username != null && !username.trim().isEmpty()) {
			// check if username is already registered
			if (!usersInfo.contains(username)) {
				String email = request.getParameter("email");
				
				// email field is mandatory
				if (email != null && !email.trim().isEmpty()) {
					// check if email is already registered
					if (!usersInfo.contains(email)) {
						// The email must be valid...
						if (validateEmail(email)) {
							// Saving information of the new registered user..
							usersInfo.add(username);
							usersInfo.add(email);
							
							ServletUtils.sendJSONSuccess(request, response, "New admin inserted with success. You will receive your password by email after the validation of a superadmin.");
						} else {
							ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST, "Email invalid format");
						}
					} else {
						ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST, "Email already registered");
					}
				} else {
					ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST, "Missing parameter: email");
				}
			} else {
				ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST, "Username already registered");
			}
		} else {
			ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST, "Missing parameter: username");
		}
	}
	
	/**
	 * Check if a email is valid
	 * @param emailStr
	 * @return
	 */
	public boolean validateEmail(String emailStr) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}
}
