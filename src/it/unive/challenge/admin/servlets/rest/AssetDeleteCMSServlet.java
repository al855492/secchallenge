package it.unive.challenge.admin.servlets.rest;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unive.challenge.ServletUtils;
import it.unive.challenge.admin.servlets.AuthenticatedServlet;

public class AssetDeleteCMSServlet extends AuthenticatedServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3274447231458955572L;
	
	@Override
	public void doAuthenticatedPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String fileName = request.getParameter("filename");
		
		if (fileName != null) {
			File deleteFile = new File(this.getServletContext().getRealPath("assets") + File.separator + fileName);
			
			String completeFileName = ("assets" + File.separator + fileName);
			
			// Check if the file to delete exists
			if (deleteFile.exists()) {
				String canonicalPath = deleteFile.getCanonicalPath();
				
				// Admins can delete only assets uploaded. No applications files
				// To do that, if we want to remove a file we first check if this one
				// is in the set uploadedFiles
				if (AssetUploadCMSServlet.uploadedFiles.contains(canonicalPath)) {
					AssetUploadCMSServlet.uploadedFiles.remove(canonicalPath);
					deleteFile.delete();
					
					ServletUtils.sendJSONSuccess(request, response,
							"Delete asset: " + completeFileName);					
				} else {
					ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_FORBIDDEN,
							"Application's files cannot be deleted");
				}
			} else {
				ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_FORBIDDEN,
						completeFileName + " not exists");
			}
		} else {
			ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST,
					"Missing parameter: filename");
		}
	
	}
}
