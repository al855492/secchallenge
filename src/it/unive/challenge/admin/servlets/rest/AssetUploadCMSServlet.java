package it.unive.challenge.admin.servlets.rest;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import it.unive.challenge.ServletUtils;
import it.unive.challenge.admin.servlets.AuthenticatedServlet;

public class AssetUploadCMSServlet extends AuthenticatedServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4530147452714328958L;

	// Set of files uploaded by users
	public static Set<String> uploadedFiles = Collections.synchronizedSet(new LinkedHashSet<>());

	@Override
	public void doAuthenticatedPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String fileName = null;
		FileItem fileUploaded = null;
		
		// File upload requires the use of a multipart form
		if (ServletFileUpload.isMultipartContent(request)) {
			try {
				List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);

				for (FileItem item : multiparts) {
					if (item.isFormField()) {
						// Reading file name
						if ("filename".equals(item.getFieldName())) {
							fileName = item.getString();
						}
					} else {
						// Reading file content
						// In case of multiple files we consider always the last one
						fileUploaded = item;
					}
				}
			} catch (FileUploadException e) {
				ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
						"something goes wrong");
			}

			// filename is mandatory
			if (fileName != null) {
				// file content is mandatory
				if (fileUploaded != null) {
					String basePath = this.getServletContext().getRealPath("");
					// the new file are uploaded inside the 'assets' folder
					File newFile = new File(this.getServletContext().getRealPath("assets") + File.separator + fileName);

					String completeFileName = ("assets" + File.separator + fileName);
					
					// We need to perform some security checks
					
					// 1) Block writing outside WebContent folder
					if (newFile.getCanonicalPath().startsWith(basePath)) {
						// 2) No possibility to overwrite existing files
						if (!newFile.exists()) {
							try {
								// Save file to disk!
								fileUploaded.write(newFile);
								
								// Saving reference to the new file uploaded
								// This file can be deleted using an admin service
								uploadedFiles.add(newFile.getCanonicalPath());

								ServletUtils.sendJSONSuccess(request, response,
										"Upload completed with success! The new asset has been saved in: "
												+ completeFileName);
							} catch (Exception e) {
								ServletUtils.sendJSONError(request, response,
										HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "something goes wrong");
							}

						} else {
							ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_FORBIDDEN,
									completeFileName + " already exists");
						}
					} else {
						ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_FORBIDDEN,
								completeFileName + " is outside the WebContent directory");
					}
				} else {
					ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST,
							"Missing file content");
				}
			} else {
				ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST,
						"Missing parameter: filename");
			}
		} else {
			ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_BAD_REQUEST,
					"No multipart/form-data found");
		}
	}

}
