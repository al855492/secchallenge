package it.unive.challenge.admin.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unive.challenge.ServletUtils;

public class LoginCMSServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3703918361475516370L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// When this servlet is called the browser will render the content of login.jsp file
		ServletUtils.sendForward(request, response, "/SecureCMS/login.jsp");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// This is a fake login form because the real authentication is obtained through a secret header
		// So we always send an error message		
		ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_OK, "Wrong username or password!");
	}
}
