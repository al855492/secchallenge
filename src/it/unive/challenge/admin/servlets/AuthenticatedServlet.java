package it.unive.challenge.admin.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import it.unive.challenge.ServletUtils;

public class AuthenticatedServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2443231775472680566L;

	private static final String secureKey = "X-API-KEY";
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Checking is user is authenticated
		if (this.isAuthenticated(request, response)) {
			doAuthenticatedGet(request, response);
		} else sendNoAuthenticationError(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Checking is user is authenticated		
		if (this.isAuthenticated(request, response)) {
			doAuthenticatedPost(request, response);
		} else sendNoAuthenticationError(request, response);
	}
	
	/**
	 * Get method to be perfomed if the user is authenticated
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void doAuthenticatedGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
	}
	
	/**
	 * Post method to be performed if the user is authenticated
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void doAuthenticatedPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
	}	
	
	private boolean isAuthenticated(HttpServletRequest request, HttpServletResponse response) {
		String xApiKey = getServletContext().getInitParameter(secureKey);
		
		// The authenticated services requires as input a secret header 'X-API-KEY'
		// The valid value of this header is stored inside web.xml (in this way no one can find the secret value...)		
		return xApiKey.equals(request.getHeader(secureKey));
	}
	
	private void sendNoAuthenticationError(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ServletUtils.sendJSONError(request, response, HttpServletResponse.SC_FORBIDDEN, "Invalid or missing header: " + secureKey);
	}
}
