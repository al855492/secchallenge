package it.unive.challenge.admin.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SecureCMSFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest rqs = (HttpServletRequest) request;
		HttpServletResponse rsp = (HttpServletResponse) response;
		
		// Reading current servlet path
		String action = rqs.getServletPath();
		
		// All request must pass through a servlet that validates the user's roles (except for assets resources)
		if (action == null || (!action.endsWith(".do") && !action.startsWith("/SecureCMS/assets/"))) {
			// Otherwise send to login servlet!
			rsp.sendRedirect(rqs.getContextPath() + "/SecureCMS/login.do");
		} else chain.doFilter(request, response);		
	}

}
