<!--================ Hero sm Banner start =================-->      
 <section class="hero-banner hero-banner--sm mb-30px">
   <div class="container">
     <div class="hero-banner--sm__content">
       <h1>Pricing Plan</h1>
       <nav aria-label="breadcrumb" class="banner-breadcrumb">
         <ol class="breadcrumb">
           <li class="breadcrumb-item"><a href="portal.do?page=home">Home</a></li>
           <li class="breadcrumb-item active" aria-current="page">Pricing Plan</li>
         </ol>
       </nav>
     </div>
   </div>
 </section>
 <!--================ Hero sm Banner end =================-->

 
<!--================ Pricing section start =================-->      
<section class="section-margin">
  <div class="container">
    <div class="section-intro pb-85px text-center">
      <h2 class="section-intro__title">Choose Your Plan</h2>
    </div>

    <div class="row">
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <div class="card text-center card-pricing">
          <div class="card-pricing__header">
            <h4>Normal</h4>
            <h1 class="card-pricing__price"><span>$</span>99.00</h1>
          </div>
          <ul class="card-pricing__list">
            <li><i class="ti-check"></i>HTTPS Hosting</li>
            <li><i class="ti-check"></i>SecureCMS</li>
            <li><i class="ti-check"></i>Advanced Firewall</li>
            <li class="unvalid"><i class="ti-close"></i>Intrusion Detection System</li>
            <li class="unvalid"><i class="ti-close"></i>Log Analysis With Machine Learning</li>
          </ul>
          <div class="card-pricing__footer">
            <button class="button button-light">Buy Now</button>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <div class="card text-center card-pricing">
          <div class="card-pricing__header">
            <h4>Business</h4>
            <h1 class="card-pricing__price"><span>$</span>499.00</h1>
          </div>
          <ul class="card-pricing__list">
            <li><i class="ti-check"></i>HTTPS Hosting</li>
            <li><i class="ti-check"></i>SecureCMS</li>
            <li><i class="ti-check"></i>Advanced Firewall</li>
            <li><i class="ti-check"></i>Intrusion Detection System</li>
            <li class="unvalid"><i class="ti-close"></i>Log Analysis With Machine Learning</li>
          </ul>
          <div class="card-pricing__footer">
            <button class="button button-light">Buy Now</button>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <div class="card text-center card-pricing">
          <div class="card-pricing__header">
            <h4>Enterprise</h4>
            <h1 class="card-pricing__price"><span>$</span>1999.00</h1>
          </div>
          <ul class="card-pricing__list">
            <li><i class="ti-check"></i>HTTPS Hosting</li>
            <li><i class="ti-check"></i>SecureCMS</li>
            <li><i class="ti-check"></i>Advanced Firewall</li>
            <li><i class="ti-check"></i>Intrusion Detection System</li>
            <li><i class="ti-check"></i>Log Analysis With Machine Learning</li>
          </ul>
          <div class="card-pricing__footer">
            <button class="button button-light">Buy Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--================ Pricing section end =================--> 
 



 <!--================ Testimonial section start =================-->      
<section class="section-padding bg-magnolia">
  <div class="container">
    <div class="section-intro pb-5 text-center">
      <h2 class="section-intro__title">Client Says About Us</h2>
      <p class="section-intro__subtitle">Find out what our customers think about our tools </p>
    </div>

    <div class="owl-carousel owl-theme testimonial">
      <div class="testimonial__item text-center">
        <div class="testimonial__img">
          <img src="assets.do?file=/img/testimonial/testimonial1.png" alt="">
        </div>
        <div class="testimonial__content">
          <h3>Stephen Mcmilan</h3>
          <p>Executive, WGroup</p>
          <p class="testimonial__i">CyberSekTek allowed me to save my company from cyber attacks that could have led to bankruptcy.</p>
        </div>
      </div>
      <div class="testimonial__item text-center">
        <div class="testimonial__img">
          <img src="assets.do?file=/img/testimonial/testimonial1.png" alt="">
        </div>
        <div class="testimonial__content">
          <h3>Joseph Musk</h3>
          <p>Executive, TGroup</p>
          <p class="assets.do?file=/testimonial__i">We use CyberSekTek to manage our automotive backend systems. We feel completely safe from any threat.</p>
        </div>
      </div>
      <div class="testimonial__item text-center">
        <div class="testimonial__img">
          <img src="assets.do?file=/img/testimonial/testimonial1.png" alt="">
        </div>
        <div class="testimonial__content">
          <h3>Manuel Donet</h3>
          <p>Executive, GGroup</p>
          <p class="testimonial__i">We are the biggest insurance conglomerate in the world and our customers feel safe also thanks to CyberSekTek.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!--================ Testimonial section end =================-->          


 <!--================ Start Clients Logo Area =================-->
<section class="clients_logo_area section-padding">
	<div class="container">
		<div class="clients_slider owl-carousel">
			<div class="item">
				<img src="assets.do?file=/img/clients-logo/c-logo-1.png" alt="">
			</div>
			<div class="item">
				<img src="assets.do?file=/img/clients-logo/c-logo-2.png" alt="">
			</div>
			<div class="item">
				<img src="assets.do?file=/img/clients-logo/c-logo-3.png" alt="">
			</div>
			<div class="item">
				<img src="assets.do?file=/img/clients-logo/c-logo-4.png" alt="">
			</div>
			<div class="item">
				<img src="assets.do?file=/img/clients-logo/c-logo-5.png" alt="">
			</div>
		</div>
	</div>
</section>
<!--================ End Clients Logo Area =================-->