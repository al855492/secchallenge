 <!--================ Hero sm Banner start =================-->      
 <section class="hero-banner hero-banner--sm mb-30px">
   <div class="container">
     <div class="hero-banner--sm__content">
       <h1>Our Features</h1>
       <nav aria-label="breadcrumb" class="banner-breadcrumb">
         <ol class="breadcrumb">
           <li class="breadcrumb-item"><a href="portal.do?page=home">Home</a></li>
           <li class="breadcrumb-item active" aria-current="page">Our Feature</li>
         </ol>
       </nav>
     </div>
   </div>
 </section>
 <!--================ Hero sm Banner end =================-->

 
 <!--================ Offer section start =================-->      
 <section class="section-margin">
   <div class="container">
     <div class="section-intro pb-85px text-center">
       <h2 class="section-intro__title">Features We Offer</h2>
     </div>

     <div class="row">
       <div class="col-lg-6">

         <div class="row offer-single-wrapper">
           <div class="col-lg-6 offer-single">
             <div class="card offer-single__content text-center">
               <span class="offer-single__icon">
                 <i class="ti-pencil-alt"></i>
               </span>
               <h4>Easy To Manage</h4>
               <p>Through our user interfaces you can easily perform any operation.</p>
             </div>
           </div>
           
           <div class="col-lg-6 offer-single">
             <div class="card offer-single__content text-center">
               <span class="offer-single__icon">
                 <i class="ti-ruler-pencil"></i>
               </span>
               <h4>Analytics Tool</h4>
               <p>Customized reports for all needs.</p>
             </div>
           </div>
         </div>

         <div class="row offer-single-wrapper">
           <div class="col-lg-6 offer-single">
             <div class="card offer-single__content text-center">
               <span class="offer-single__icon">
                 <i class="ti-cut"></i>
               </span>
               <h4>Professionals Tools</h4>
               <p>You will have access to the most advanced tools available on the market.</p>
             </div>
           </div>
           
           <div class="col-lg-6 offer-single">
             <div class="card offer-single__content text-center">
               <span class="offer-single__icon">
                 <i class="ti-light-bulb"></i>
               </span>
               <h4>Ready Content</h4>
               <p>All tools are already configured to block major threats.</p>
             </div>
           </div>
         </div>

       </div>
       <div class="col-lg-6">
         <div class="offer-single__img">
           <img class="img-fluid" src="assets.do?file=/img/home/offer.png" alt="">
         </div>
       </div>
     </div>
   </div>
 </section>
 <!--================ Offer section end =================-->
 



<!--================ Testimonial section start =================-->      
<section class="section-padding bg-magnolia">
  <div class="container">
    <div class="section-intro pb-5 text-center">
      <h2 class="section-intro__title">Client Says About Us</h2>
      <p class="section-intro__subtitle">Find out what our customers think about our tools </p>
    </div>

    <div class="owl-carousel owl-theme testimonial">
      <div class="testimonial__item text-center">
        <div class="testimonial__img">
          <img src="assets.do?file=/img/testimonial/testimonial1.png" alt="">
        </div>
        <div class="testimonial__content">
          <h3>Stephen Mcmilan</h3>
          <p>Executive, WGroup</p>
          <p class="testimonial__i">CyberSekTek allowed me to save my company from cyber attacks that could have led to bankruptcy.</p>
        </div>
      </div>
      <div class="testimonial__item text-center">
        <div class="testimonial__img">
          <img src="assets.do?file=/img/testimonial/testimonial1.png" alt="">
        </div>
        <div class="testimonial__content">
          <h3>Joseph Musk</h3>
          <p>Executive, TGroup</p>
          <p class="assets.do?file=/testimonial__i">We use CyberSekTek to manage our automotive backend systems. We feel completely safe from any threat.</p>
        </div>
      </div>
      <div class="testimonial__item text-center">
        <div class="testimonial__img">
          <img src="assets.do?file=/img/testimonial/testimonial1.png" alt="">
        </div>
        <div class="testimonial__content">
          <h3>Manuel Donet</h3>
          <p>Executive, GGroup</p>
          <p class="testimonial__i">We are the biggest insurance conglomerate in the world and our customers feel safe also thanks to CyberSekTek.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!--================ Testimonial section end =================-->       


 <!--================ Start Clients Logo Area =================-->
<section class="clients_logo_area section-padding">
	<div class="container">
		<div class="clients_slider owl-carousel">
			<div class="item">
				<img src="assets.do?file=/img/clients-logo/c-logo-1.png" alt="">
			</div>
			<div class="item">
				<img src="assets.do?file=/img/clients-logo/c-logo-2.png" alt="">
			</div>
			<div class="item">
				<img src="assets.do?file=/img/clients-logo/c-logo-3.png" alt="">
			</div>
			<div class="item">
				<img src="assets.do?file=/img/clients-logo/c-logo-4.png" alt="">
			</div>
			<div class="item">
				<img src="assets.do?file=/img/clients-logo/c-logo-5.png" alt="">
			</div>
		</div>
	</div>
</section>
<!--================ End Clients Logo Area =================-->
