<%
	String js_nonce = (String) request.getAttribute("js_nonce");
%>
<!--================ Hero sm Banner start =================-->      
<section class="hero-banner hero-banner--sm mb-30px">
  <div class="container">
    <div class="hero-banner--sm__content">
      <h1>Contact Us</h1>
      <nav aria-label="breadcrumb" class="banner-breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="portal.do?page=home">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<!--================ Hero sm Banner end =================-->


<!-- ================ contact section start ================= -->
<section class="section-margin">
  <div class="container">
    <div class="d-none d-sm-block mb-5 pb-4">
      <div id="map" class="contact-map">
      	<img src="assets.do?file=/img/map.png" class="img-responsive" />
      </div>
    </div>


    <div class="row">
      <div class="col-md-4 col-lg-3 mb-4 mb-md-0">
        <div class="media contact-info">
          <span class="contact-info__icon"><i class="ti-home"></i></span>
          <div class="media-body">
            <h3>Panama</h3>
            <p>Calle 50</p>
          </div>
        </div>
        <div class="media contact-info">
          <span class="contact-info__icon"><i class="ti-headphone"></i></span>
          <div class="media-body">
            <h3><a href="tel:454545654">00 (999) 9999 999</a></h3>
            <p>Mon to Sun 8am to 6pm</p>
          </div>
        </div>
        <div class="media contact-info">
          <span class="contact-info__icon"><i class="ti-email"></i></span>
          <div class="media-body">
            <h3><a href="mailto:support@cybersektek.com">support@cybersektek.com</a></h3>
            <p>Send us your query anytime!</p>
          </div>
        </div>
      </div>
      <div class="col-md-8 col-lg-9">
        <form class="form-contact contact_form" method="get" id="contactForm">
          <div class="row">
            <div class="col-lg-5">
              <div class="form-group">
                <input class="form-control" id="name" type="text" placeholder="Enter your name">
              </div>
              <div class="form-group">
                <input class="form-control" id="email" type="email" placeholder="Enter email address">
              </div>
              <div class="form-group">
                <input class="form-control" id="subject" type="text" placeholder="Enter Subject">
              </div>
            </div>
            <div class="col-lg-7">
              <div class="form-group">
                  <textarea class="form-control different-control w-100" name="message" id="message" cols="30" rows="5" placeholder="Enter Message"></textarea>
              </div>
            </div>
          </div>
          <div class="form-group text-center text-md-right mt-3">
            <button disabled="disabled" type="submit" class="button button-contactForm">Send Message</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<!-- ================ contact section end ================= -->