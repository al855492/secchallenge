<main class="side-main">
  <!--================ Hero sm Banner start =================-->      
<section class="hero-banner mb-30px">
  <div class="container">
    <div class="row">
      <div class="col-lg-7">
        <div class="hero-banner__img">
          <img class="img-fluid" src="assets.do?file=/img/banner/hero-banner.png" alt="">
        </div>
      </div>
      <div class="col-lg-5 pt-5">
        <div class="hero-banner__content">
          <h1>Advanced security made simple</h1>
          <p>Our goal is to protect your business from digital threats. We have packages and solutions suitable for any situation.</p>
          <a class="button bg" href="#">Get Started</a>
        </div>
      </div>
    </div>
  </div>
</section>
<!--================ Hero sm Banner end =================-->

<!--================ Feature section start =================-->      
<section class="section-margin">
  <div class="container">
    <div class="section-intro pb-85px text-center">
      <h2 class="section-intro__title">Awesome Solutions For You</h2>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-lg-4">
          <div class="card card-feature text-center text-lg-left mb-4 mb-lg-0">
            <span class="card-feature__icon">
              <i class="ti-package"></i>
            </span>
            <h3 class="card-feature__title">Business Solution</h3>
            <p class="card-feature__subtitle">Standard solutions suitable for small businesses</p>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="card card-feature text-center text-lg-left mb-4 mb-lg-0">
            <span class="card-feature__icon">
              <i class="ti-mouse-alt"></i>
            </span>
            <h3 class="card-feature__title">Enterprise Solution</h3>
            <p class="card-feature__subtitle">Customizable solutions based on the needs of your company</p>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="card card-feature text-center text-lg-left mb-4 mb-lg-0">
            <span class="card-feature__icon">
              <i class="ti-headphone-alt"></i>
            </span>
            <h3 class="card-feature__title">Customer Support</h3>
            <p class="card-feature__subtitle">Technical Helpdesk available 24/7/365 for any problem</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--================ Feature section end =================-->      

<!--================ about section start =================-->      
<section class="section-padding--small bg-magnolia">
  <div class="container">
    <div class="row no-gutters align-items-center">
      <div class="col-md-5 mb-5 mb-md-0">
        <div class="about__content">
          <h2>Keep Track Of All Your Digital Assets</h2>
          <p>Through our awesome dashboards you will be always updated on the security status of your digital assets</p>
          <a class="button button-light" href="#">Know More</a>
        </div>
      </div>
      <div class="col-md-7">
        <div class="about__img">
          <img class="img-fluid" src="assets.do?file=/img/home/about.png" alt="">
        </div>
      </div>
    </div>
  </div>
</section>
<!--================ about section end =================-->      

<!--================ Offer section start =================-->      
<section class="section-margin">
  <div class="container">
    <div class="section-intro pb-85px text-center">
      <h2 class="section-intro__title">Features We Offer</h2>
    </div>

    <div class="row">
      <div class="col-lg-6">

        <div class="row offer-single-wrapper">
          <div class="col-lg-6 offer-single">
            <div class="card offer-single__content text-center">
              <span class="offer-single__icon">
                <i class="ti-pencil-alt"></i>
              </span>
              <h4>Easy To Manage</h4>
              <p>Through our user interfaces you can easily perform any operation.</p>
            </div>
          </div>
          
          <div class="col-lg-6 offer-single">
            <div class="card offer-single__content text-center">
              <span class="offer-single__icon">
                <i class="ti-ruler-pencil"></i>
              </span>
              <h4>Analytics Tool</h4>
              <p>Customized reports for all needs.</p>
            </div>
          </div>
        </div>

        <div class="row offer-single-wrapper">
          <div class="col-lg-6 offer-single">
            <div class="card offer-single__content text-center">
              <span class="offer-single__icon">
                <i class="ti-cut"></i>
              </span>
              <h4>Professionals Tools</h4>
              <p>You will have access to the most advanced tools available on the market.</p>
            </div>
          </div>
          
          <div class="col-lg-6 offer-single">
            <div class="card offer-single__content text-center">
              <span class="offer-single__icon">
                <i class="ti-light-bulb"></i>
              </span>
              <h4>Ready Content</h4>
              <p>All tools are already configured to block major threats.</p>
            </div>
          </div>
        </div>

      </div>
      <div class="col-lg-6">
        <div class="offer-single__img">
          <img class="img-fluid" src="assets.do?file=/img/home/offer.png" alt="">
        </div>
      </div>
    </div>
  </div>
</section>
<!--================ Offer section end =================-->      

<!--================ Solution section start =================-->      
<section class="section-padding--small bg-magnolia">
  <div class="container">
    <div class="row align-items-center pt-xl-3 pb-xl-5">
      <div class="col-lg-6">
        <div class="solution__img text-center text-lg-left mb-4 mb-lg-0">
          <img class="img-fluid" src="assets.do?file=/img/home/solution.png" alt="">
        </div>
      </div>
      <div class="col-lg-6">
        <div class="solution__content">
          <h2>Simple Solutions for Advanced Threats</h2>
          <p>Blocking the most advanced threats will seem like child's play</p>
          <a class="button button-light" href="#">Know More</a>
        </div>
      </div>
    </div>
  </div>
</section>
<!--================ Solution section end =================-->      

<!--================ Pricing section start =================-->      
<section class="section-margin">
  <div class="container">
    <div class="section-intro pb-85px text-center">
      <h2 class="section-intro__title">Choose Your Plan</h2>
    </div>

    <div class="row">
      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <div class="card text-center card-pricing">
          <div class="card-pricing__header">
            <h4>Normal</h4>
            <h1 class="card-pricing__price"><span>$</span>99.00</h1>
          </div>
          <ul class="card-pricing__list">
            <li><i class="ti-check"></i>HTTPS Hosting</li>
            <li><i class="ti-check"></i>SecureCMS</li>
            <li><i class="ti-check"></i>Advanced Firewall</li>
            <li class="unvalid"><i class="ti-close"></i>Intrusion Detection System</li>
            <li class="unvalid"><i class="ti-close"></i>Log Analysis With Machine Learning</li>
          </ul>
          <div class="card-pricing__footer">
            <button class="button button-light">Buy Now</button>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <div class="card text-center card-pricing">
          <div class="card-pricing__header">
            <h4>Business</h4>
            <h1 class="card-pricing__price"><span>$</span>499.00</h1>
          </div>
          <ul class="card-pricing__list">
            <li><i class="ti-check"></i>HTTPS Hosting</li>
            <li><i class="ti-check"></i>SecureCMS</li>
            <li><i class="ti-check"></i>Advanced Firewall</li>
            <li><i class="ti-check"></i>Intrusion Detection System</li>
            <li class="unvalid"><i class="ti-close"></i>Log Analysis With Machine Learning</li>
          </ul>
          <div class="card-pricing__footer">
            <button class="button button-light">Buy Now</button>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
        <div class="card text-center card-pricing">
          <div class="card-pricing__header">
            <h4>Enterprise</h4>
            <h1 class="card-pricing__price"><span>$</span>1999.00</h1>
          </div>
          <ul class="card-pricing__list">
            <li><i class="ti-check"></i>HTTPS Hosting</li>
            <li><i class="ti-check"></i>SecureCMS</li>
            <li><i class="ti-check"></i>Advanced Firewall</li>
            <li><i class="ti-check"></i>Intrusion Detection System</li>
            <li><i class="ti-check"></i>Log Analysis With Machine Learning</li>
          </ul>
          <div class="card-pricing__footer">
            <button class="button button-light">Buy Now</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--================ Pricing section end =================-->      

<!--================ Testimonial section start =================-->      
<section class="section-padding bg-magnolia">
  <div class="container">
    <div class="section-intro pb-5 text-center">
      <h2 class="section-intro__title">Client Says About Us</h2>
      <p class="section-intro__subtitle">Find out what our customers think about our tools </p>
    </div>

    <div class="owl-carousel owl-theme testimonial">
      <div class="testimonial__item text-center">
        <div class="testimonial__img">
          <img src="assets.do?file=/img/testimonial/testimonial1.png" alt="">
        </div>
        <div class="testimonial__content">
          <h3>Stephen Mcmilan</h3>
          <p>Executive, WGroup</p>
          <p class="testimonial__i">CyberSekTek allowed me to save my company from cyber attacks that could have led to bankruptcy.</p>
        </div>
      </div>
      <div class="testimonial__item text-center">
        <div class="testimonial__img">
          <img src="assets.do?file=/img/testimonial/testimonial1.png" alt="">
        </div>
        <div class="testimonial__content">
          <h3>Joseph Musk</h3>
          <p>Executive, TGroup</p>
          <p class="assets.do?file=/testimonial__i">We use CyberSekTek to manage our automotive backend systems. We feel completely safe from any threat.</p>
        </div>
      </div>
      <div class="testimonial__item text-center">
        <div class="testimonial__img">
          <img src="assets.do?file=/img/testimonial/testimonial1.png" alt="">
        </div>
        <div class="testimonial__content">
          <h3>Manuel Donet</h3>
          <p>Executive, GGroup</p>
          <p class="testimonial__i">We are the biggest insurance conglomerate in the world and our customers feel safe also thanks to CyberSekTek.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<!--================ Testimonial section end =================-->      


<!--================ Start Clients Logo Area =================-->
<section class="clients_logo_area section-padding">
  <div class="container">
    <div class="clients_slider owl-carousel">
      <div class="item">
        <img src="assets.do?file=/img/clients-logo/c-logo-1.png" alt="">
      </div>
      <div class="item">
        <img src="assets.do?file=/img/clients-logo/c-logo-2.png" alt="">
      </div>
      <div class="item">
        <img src="assets.do?file=/img/clients-logo/c-logo-3.png" alt="">
      </div>
      <div class="item">
        <img src="assets.do?file=/img/clients-logo/c-logo-4.png" alt="">
      </div>
      <div class="item">
        <img src="assets.do?file=/img/clients-logo/c-logo-5.png" alt="">
      </div>
    </div>
  </div>
</section>
<!--================ End Clients Logo Area =================-->
</main>