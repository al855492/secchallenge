<%
	String include = request.getParameter("include");

	// Preparing Content-Security-Policy
	String js_nonce = java.util.Base64.getEncoder().encodeToString(Long.toHexString(Double.doubleToLongBits(Math.random())).getBytes());
	
	request.setAttribute("js_nonce", js_nonce);
	
	response.setHeader("Content-Security-Policy", 
		"default-src 'self'; " +
		"script-src 'self' 'nonce-" + js_nonce + "'; " +
		"style-src 'self' https://*.googleapis.com 'unsafe-inline'; " +
		"font-src 'self' https://*.googleapis.com https://*.gstatic.com data: ;"
	);
	
%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>CyberSekTek - <%=include.toUpperCase()%></title>
  <link rel="icon" href="assets.do?file=/img/Favicon.png" type="image/png">

  <link rel="stylesheet" href="assets.do?file=/vendors/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="assets.do?file=/vendors/fontawesome/css/all.min.css">
  <link rel="stylesheet" href="assets.do?file=/vendors/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="assets.do?file=/vendors/linericon/style.css">
  <link rel="stylesheet" href="assets.do?file=/vendors/owl-carousel/owl.theme.default.min.css">
  <link rel="stylesheet" href="assets.do?file=/vendors/owl-carousel/owl.carousel.min.css">

  <link rel="stylesheet" href="assets.do?file=/css/style.css">
</head>
<body>
  <!--================Header Menu Area =================-->
  <header class="header_area">
    <div class="main_menu">
      <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container box_1620">
          <!-- Brand and toggle get grouped for better mobile display -->
          <a class="navbar-brand logo_h" href="portal.do?page=home"><img src="assets.do?file=/img/logo.png" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
            <ul class="nav navbar-nav menu_nav justify-content-end">
              <li class="nav-item <%= ("home".equals(include) ? "active" : "") %>"><a class="nav-link" href="portal.do?page=home">Home</a></li> 
              <li class="nav-item <%= ("feature".equals(include) ? "active" : "") %>"><a class="nav-link" href="portal.do?page=feature">Feature</a></li> 
              <li class="nav-item <%= ("pricing".equals(include) ? "active" : "") %>"><a class="nav-link" href="portal.do?page=pricing">Price</a>
              <li class="nav-item <%= ("contact".equals(include) ? "active" : "") %>"><a class="nav-link" href="portal.do?page=contact">Contact</a></li>
            </ul>
          </div> 
        </div>
      </nav>
    </div>
  </header>
  <!--================Header Menu Area =================-->
  
  <!--================ Dynamic Content =================-->
  <% out.write("<!-- Reading page file " + include + ".jsp from /pages/ subfolders -->\n"); %>  	
  <% pageContext.include("/pages/" + include + ".jsp"); %>

  <!-- ================ start footer Area ================= -->
  <footer class="footer-area section-gap">
		<div class="container">
			<div class="row">
				<div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
					<h4>Top Products</h4>
					<ul>
						<li><a href="#">Hosting HTTPS</a></li>
						<li><a href="#">SecureCMS</a></li>
						<li><a href="#">Advanced Firewall</a></li>
						<li><a href="#">Intrusion Detection System</a></li>
					</ul>
				</div>
				<div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
					<h4>Quick Links</h4>
					<ul>
						<li><a href="#">Jobs</a></li>
						<li><a href="#">Brand Assets</a></li>
						<li><a href="#">Investor Relations</a></li>
						<li><a href="#">Terms of Service</a></li>
					</ul>
				</div>
				<div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
					<h4>Features</h4>
					<ul>
						<li><a href="#">Jobs</a></li>
						<li><a href="#">Brand Assets</a></li>
						<li><a href="#">Investor Relations</a></li>
						<li><a href="#">Terms of Service</a></li>
					</ul>
				</div>
				<div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
					<h4>Resources</h4>
					<ul>
						<li><a href="#">Guides</a></li>
						<li><a href="#">Research</a></li>
						<li><a href="#">Experts</a></li>
						<li><a href="#">Agencies</a></li>
					</ul>
				</div>
				<div class="col-xl-4 col-md-8 mb-4 mb-xl-0 single-footer-widget">
					<h4>Newsletter</h4>
					<p>You can trust us. we only send promo offers,</p>
					<div class="form-wrap" id="mc_embed_signup">
						<form target="_blank" class="form-inline" onsubmit="return false">
							<input class="form-control" name="EMAIL" placeholder="Your Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '"
							 required="required" type="email">
							<button class="click-btn btn btn-default">subscribe</button>
							<div class="info"></div>
						</form>
					</div>
				</div>
			</div>
			<div class="footer-bottom row align-items-center text-center text-lg-left">
				<p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script nonce="<%=js_nonce%>">document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				<div class="col-lg-4 col-md-12 text-center text-lg-right footer-social">
					<a href="#"><i class="fab fa-facebook-f"></i></a>
					<a href="#"><i class="fab fa-twitter"></i></a>
					<a href="#"><i class="fab fa-dribbble"></i></a>
					<a href="#"><i class="fab fa-behance"></i></a>
				</div>
			</div>
		</div>
		
		<div class="footer-credits container">
			<p>Powered by&nbsp;<a href="SecureCMS/">SecureCMS</a></p>
		</div>
	</footer>
  <!-- ================ End footer Area ================= -->

  <script src="assets.do?file=/vendors/jquery/jquery-3.2.1.min.js"></script>
  <script src="assets.do?file=/vendors/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="assets.do?file=/vendors/owl-carousel/owl.carousel.min.js"></script>
  <script src="assets.do?file=/js/main.js"></script>
</body>
</html>